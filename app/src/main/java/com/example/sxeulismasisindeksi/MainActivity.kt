package com.example.sxeulismasisindeksi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initListeners()
    }

    private fun initListeners() {
        gamotvlis_gilaki.setOnClickListener {
            onClick()
        }
    }

    private fun onClick() {
        var weight = wona.text.toString().toFloat()
        var height = simagle.text.toString().toFloat()
        var bmi = calculate(weight, height)
        var result = shedareba(bmi)
        shedegi.text = result
    }

    private fun calculate(weight: Float, height: Float) = weight / ((height / 100) * (height / 100))

    private fun shedareba(BMI: Float): String {
        var diagnostic = when (BMI) {
            in 0f..16f -> "ძლიერი სიგამხდრე"
            in 16f..17f -> "ზომიერი სიგამხდრე"
            in 17f..18.5f -> "მსუბუქი სიგამხდრე"
            in 18.5f..25f -> "ნორმალური"
            in 25f..30f -> "ჭარბი წონა"
            in 30f..35f -> "სიმსუქნის კლასი I"
            in 35f..40f -> "სიმსუქნის კლასი II"
            else -> "სიმსუქნის კლასი III"
        }

        return diagnostic
    }
}